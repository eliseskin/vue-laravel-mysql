<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichadeprestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichadeprestamos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('folio');
            $table->string('tipodeprestamo', 30);
            $table->date('fechaprestamo');
            $table->date('fechadevolucion');
            $table->integer('matricula');
            $table->integer('nocontrol');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichadeprestamos');
    }
}
