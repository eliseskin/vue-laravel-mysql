<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos_personal extends Model
{
    protected $fillable = [
        'matricula',
        'nombre',
        'apellido',
        'sexo',
        'grado',
        'grupo',
        'direccion',
        'ocupacion',
        'telefono'
    ];
}
