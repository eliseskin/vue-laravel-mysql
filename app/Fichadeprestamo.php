<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fichadeprestamo extends Model
{
    public function alumno() {
        return $this->belongsTo(Alumnos_personal::class, 'matricula', 'id');
    }

    public function libro() {
        return $this->belongsTo(Libro::class, 'nocontrol', 'id');
    }

    protected $fillable = [
        'folio',
        'tipodeprestamo',
        'fechaprestamo',
        'fechadevolucion',
        'matricula',
        'nocontrol',
    ];
}
