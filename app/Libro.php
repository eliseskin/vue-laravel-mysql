<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $fillable = [
        'nocontrol',
        'nombre',
        'autor',
        'coleccion',
        'editorial',
        'clasificacion',
    ];
}
