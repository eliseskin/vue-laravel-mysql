<?php

namespace App\Http\Controllers;

use App\Alumnos_personal;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;

class AlumnosPersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $alumnos = Alumnos_personal::all();
            return $alumnos;
        } else {
            return view('alumnos');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumno = new Alumnos_personal();
        $alumno->matricula = $request->matricula;
        $alumno->nombre = $request->nombre;
        $alumno->apellido = $request->apellido;
        $alumno->sexo = $request->sexo;
        $alumno->grado = $request->grado;
        $alumno->grupo = $request->grupo;
        $alumno->direccion = $request->direccion;
        $alumno->ocupacion = $request->ocupacion;
        $alumno->telefono = $request->telefono;
        $alumno->save();

        return $alumno;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumnos_personal  $alumnos_personal
     * @return \Illuminate\Http\Response
     */
    public function show(Alumnos_personal $alumnos_personal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumnos_personal  $alumnos_personal
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumnos_personal $alumnos_personal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumnos_personal  $alumnos_personal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alumno = Alumnos_personal::find($id);
        $alumno->matricula = $request->matricula;
        $alumno->nombre = $request->nombre;
        $alumno->apellido = $request->apellido;
        $alumno->sexo = $request->sexo;
        $alumno->grado = $request->grado;
        $alumno->grupo = $request->grupo;
        $alumno->direccion = $request->direccion;
        $alumno->ocupacion = $request->ocupacion;
        $alumno->telefono = $request->telefono;
        $alumno->save();

        return $alumno;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumnos_personal  $alumnos_personal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alumno = Alumnos_personal::find($id);
        $alumno->delete();
        return $alumno;
    }
}
