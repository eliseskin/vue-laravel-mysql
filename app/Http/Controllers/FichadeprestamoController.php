<?php

namespace App\Http\Controllers;

use App\Fichadeprestamo;
use Illuminate\Http\Request;

class FichadeprestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $ficha = Fichadeprestamo::all();
            return $ficha;
        } else {
            return view('prestamos');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ficha = new Fichadeprestamo();
        $ficha->folio = $request->folio;
        $ficha->tipodeprestamo = $request->tipodeprestamo;
        $ficha->fechaprestamo = $request->fechaprestamo;
        $ficha->fechadevolucion = $request->fechadevolucion;
        $ficha->matricula = $request->matricula;
        $ficha->nocontrol = $request->nocontrol;
        $ficha->save();

        return $ficha;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fichadeprestamo  $fichadeprestamo
     * @return \Illuminate\Http\Response
     */
    public function show(Fichadeprestamo $fichadeprestamo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fichadeprestamo  $fichadeprestamo
     * @return \Illuminate\Http\Response
     */
    public function edit(Fichadeprestamo $fichadeprestamo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fichadeprestamo  $fichadeprestamo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ficha = Fichadeprestamo::find($id);
        $ficha->folio = $request->folio;
        $ficha->tipodeprestamo = $request->tipodeprestamo;
        $ficha->fechaprestamo = $request->fechaprestamo;
        $ficha->fechadevolucion = $request->fechadevolucion;
        $ficha->matricula = $request->matricula;
        $ficha->nocontrol = $request->nocontrol;
        $ficha->save();

        return $ficha;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fichadeprestamo  $fichadeprestamo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ficha = Fichadeprestamo::find($id);
        $ficha->delete();
        return $ficha;
    }
}
